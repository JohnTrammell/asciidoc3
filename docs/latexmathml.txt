LaTeXMathML Formulae
====================

http://math.etsu.edu/LaTeXMathML/[LaTeXMathML]
capability has been added to AsciiDoc3 for users who are more familiar
with or prefer LaTeX math formulas to the
http://asciidoc.org/asciimathml.html[ASCIIMathML]
notation.

.Example
------------------------

python3 ../asciidoc3.py -a toc -a icons -n -a latexmath latexmathml.txt
------------------------


'LaTeXMathML' is a derivative of 'ASCIIMathML' -- in
terms of usage the only difference it that you use the `latexmath`
attribute instead of the `asciimath` attribute (see ./doc/asciimathml.txt).


'LaTeXMathML' processes LaTeX math formulas not arbitrary LaTeX (as
`dblatex(1)` does). See the
http://math.etsu.edu/LaTeXMathML/[LaTeXMathML]
website for details.

Some example 'LaTeXMathML' formulas, see 'latexmathml.txt' to look for the source.

- latexmath:[$R_x = 10.0 \times \sin(R_\phi)$]

- latexmath:[$\sum_{n=1}^\infty \frac{1}{2^n}$]

- latexmath:[$\lim_{x\to\infty} f(x) = k \choose r + \frac ab
  \sum_{n=1}^\infty a_n + \displaystyle{ \left\{ \frac{1}{13}
  \sum_{n=1}^\infty b_n \right\} }$]

- latexmath:[$\$\alpha + \$\beta = \$(\alpha + \beta)$]

- latexmath:[$\begin{eqnarray} x & = & \frac{-7 \pm
  \sqrt{49 - 24}}{6} \\ & = & -2 \textrm{ or } -\frac13.
  \end{eqnarray}$]

- latexmath:[$\displaystyle{ V_i = C_0 - C_3
  \frac{C_1\cos(\theta_i+C_3)}{C_4+C_1\cos(\theta_i+C_2)} }$]
