

usage:
	@echo "usage: "
	@echo "    make format"
	@echo "    make lint"
	@echo "    make test"
	@echo "    make install"

format fmt:
	poetry run black src/

lint:
	-poetry run ruff check src/
	-poetry run flake8 src/
	-poetry run mypy src/
	-poetry run pydocstyle src/

test:
	poetry run pytest tests/

install:
	poetry install --with=dev,test




