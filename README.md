# asciidoc3

## Text based document generation using Python 3

AsciiDoc3 is a text document format for writing notes, documentation, articles,
books, ebooks, slideshows, web pages, man pages and blogs. AsciiDoc3 files can
be translated to many formats including HTML, PDF, EPUB, man page, and DocBook
markup (at your choice v4.5 or v5.1). AsciiDoc3 is highly configurable: both
the AsciiDoc3 source file syntax and the backend output markups (which can be
almost any type of SGML/XML markup) can be customized and extended by the user.

## Copying

Copyright © 2018-2023 Berthold Gehrke <berthold.gehrke@gmail.com> for AsciiDoc3
(Python3) [Copyright © Stuart Rackham (and contributors) for AsciiDoc v8.6.9
(Python2; End-Of-Life since Jan. 2020)]  

Free use of this software is granted under the terms of the  GNU Affero General
Public License version 3 or later (AGPLv3+).
