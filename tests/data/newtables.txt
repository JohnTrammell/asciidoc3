AsciiDoc3 New tables
===================

Here are some examples of AsciiDoc3 'new
tables':

.Simple table
[width="15%"]
|=======
|1 |2 |A
|3 |4 |B
|5 |6 |C
|=======

.AsciiDoc3 source
---------------------------------------------------------------------
[width="15%"]
|=======
|1 |2 |A
|3 |4 |B
|5 |6 |C
|=======
---------------------------------------------------------------------


.Table with title, header and footer
[width="40%",frame="topbot",options="header,footer"]
|======================
|Column 1 |Column 2
|1        |Item 1
|2        |Item 2
|3        |Item 3
|6        |Three items
|======================

.AsciiDoc3 source
---------------------------------------------------------------------
.An example table
[width="40%",frame="topbot",options="header,footer"]
|======================
|Column 1 |Column 2
|1        |Item 1
|2        |Item 2
|3        |Item 3
|6        |Three items
|======================
---------------------------------------------------------------------


.Columns formatted with strong, monospaced and emphasis styles
[width="50%",cols=">s,^2m,^2e",frame="topbot",options="header,footer"]
|==========================
|      2+|Columns 2 and 3
|1       |Item 1  |Item 1
|2       |Item 2  |Item 2
|3       |Item 3  |Item 3
|4       |Item 4  |Item 4
|footer 1|footer 2|footer 3
|==========================

.AsciiDoc3 source
---------------------------------------------------------------------
.An example table
[width="50%",cols=">s,^2m,^2e",frame="topbot",options="header,footer"]
|==========================
|      2+|Columns 2 and 3
|1       |Item 1  |Item 1
|2       |Item 2  |Item 2
|3       |Item 3  |Item 3
|4       |Item 4  |Item 4
|footer 1|footer 2|footer 3
|==========================
---------------------------------------------------------------------

.A table with externally sourced CSV data
[format="csv",cols="^1,4*2",options="header"]
|===================================================
ID,Customer Name,Contact Name,Customer Address,Phone
include::customers.csv[]
|===================================================

.AsciiDoc3 source
---------------------------------------------------------------------
 [format="csv",cols="^1,4*2",options="header"]
 |===================================================
 ID,Customer Name,Contact Name,Customer Address,Phone
 include::customers.csv[]
 |===================================================
---------------------------------------------------------------------


.DVS formatted table
[width="70%",format="dsv"]
|====================================
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/bin/sh
bin:x:2:2:bin:/bin:/bin/sh
sys:x:3:3:sys:/dev:/bin/sh
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/bin/sh
|====================================

.AsciiDoc3 source
---------------------------------------------------------------------
[width="70%",format="dsv"]
|====================================
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/bin/sh
bin:x:2:2:bin:/bin:/bin/sh
sys:x:3:3:sys:/dev:/bin/sh
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/bin/sh
|====================================
---------------------------------------------------------------------


.Horizontal and vertical source data
[width="80%",cols="3,^2,^2,10",options="header"]
|=========================================================
|Date |Duration |Avg HR |Notes

|22-Aug-08 |10:24 | 157 |
Worked out MSHR (max sustainable heart rate) by going hard
for this interval.

|22-Aug-08 |23:03 | 152 |
Back-to-back with previous interval.

|24-Aug-08 |40:00 | 145 |
Moderately hard interspersed with 3x 3min intervals (2min
hard + 1min really hard taking the HR up to 160).

|=========================================================

Short cells can be entered horizontally, longer cells vertically.  The
default behavior is to strip leading and trailing blank lines within a
cell. These characteristics aid readability and data entry.

.AsciiDoc3 source
---------------------------------------------------------------------
.Windtrainer workouts
[width="80%",cols="3,^2,^2,10",options="header"]
|=========================================================
|Date |Duration |Avg HR |Notes

|22-Aug-08 |10:24 | 157 |
Worked out MSHR (max sustainable heart rate) by going hard
for this interval.

|22-Aug-08 |23:03 | 152 |
Back-to-back with previous interval.

|24-Aug-08 |40:00 | 145 |
Moderately hard interspersed with 3x 3min intervals (2min
hard + 1min really hard taking the HR up to 160).

|=========================================================
---------------------------------------------------------------------


.Default and verse styles
[cols=",^v",options="header"]
|===================================
|Default paragraphs |Centered verses
2*|Per id.

Consul *necessitatibus* per id,
consetetur, eu pro everti postulant
homero verear ea mea, qui.

Consul *necessitatibus* per id,
consetetur, eu pro everti postulant
homero verear ea mea, qui.
|===================================

.AsciiDoc3 source
---------------------------------------------------------------------
[cols=",^v",options="header"]
|===================================
|Default paragraphs |Centered verses
2*|Per id.

Consul *necessitatibus* per id,
consetetur, eu pro everti postulant
homero verear ea mea, qui.

Consul *necessitatibus* per id,
consetetur, eu pro everti postulant
homero verear ea mea, qui.
|===================================
---------------------------------------------------------------------


.Horizontal and vertical headings
[cols="h,4*",options="header",width="50%"]
|==================================
|      |West |Central |East | Total
|Q1    |270  |292     |342  | 904
|Q2    |322  |276     |383  | 981
|Q3    |298  |252     |274  | 824
|Q4    |344  |247     |402  | 993
|==================================

.AsciiDoc3 source
---------------------------------------------------------------------
.Horizontal and vertical headings
[cols="h,4*",options="header",width="50%"]
|==================================
|      |West |Central |East | Total
|Q1    |270  |292     |342  | 904
|Q2    |322  |276     |383  | 981
|Q3    |298  |252     |274  | 824
|Q4    |344  |247     |402  | 993
|==================================
---------------------------------------------------------------------


.AsciiDoc3 style in first column, Literal in second
[cols="asciidoc3,literal",options="header",grid="cols"]
|==================================
|Output markup |AsciiDoc3 source
2*|
Consul *necessitatibus* per id,
consetetur, eu pro everti postulant
homero verear ea mea, qui.

-----------------------------------
Consul *necessitatibus* per id,
consetetur, eu pro everti postulant
homero verear ea mea, qui.
-----------------------------------

.Code filter example
[source,python]
-----------------------------------
''' A multi-line
    comment.'''
def sub_word(mo):
    ''' Single line comment.'''
    word = mo.group('word')
    if word in keywords[language]:
        return quote + word + quote
    else:
        return word
-----------------------------------

- Lorem ipsum dolor sit amet,
  consectetuer adipiscing elit.
  * Fusce euismod commodo velit.
  * Qui in magna commodo, est labitur
    dolorum an. Est ne magna primis
    adolescens. Sit munere ponderum
    dignissim et. Minim luptatum et vel.
  * Vivamus fringilla mi eu lacus.
  * Donec eget arcu bibendum nunc
    consequat lobortis.
- Nulla porttitor vulputate libero.
  . Fusce euismod commodo velit.
  . Vivamus fringilla mi eu lacus.

|==================================

.AsciiDoc3 source
[listing]
.....................................................................
[cols="asciidoc3,literal",options="header",grid="cols"]
|==================================
|Output markup |AsciiDoc3 source
2*|
Consul *necessitatibus* per id,
consetetur, eu pro everti postulant
homero verear ea mea, qui.

-----------------------------------
Consul *necessitatibus* per id,
consetetur, eu pro everti postulant
homero verear ea mea, qui.
-----------------------------------

.Code filter example
[source,python]
-----------------------------------
''' A multi-line
    comment.'''
def sub_word(mo):
   ''' Single line comment.'''
    word = mo.group('word')
    if word in keywords[language]:
        return quote + word + quote
    else:
        return word
-----------------------------------

- Lorem ipsum dolor sit amet,
  consectetuer adipiscing elit.
  * Fusce euismod commodo velit.
  * Qui in magna commodo, est labitur
    dolorum an. Est ne magna primis
    adolescens. Sit munere ponderum
    dignissim et. Minim luptatum et vel.
  * Vivamus fringilla mi eu lacus.
  * Donec eget arcu bibendum nunc
    consequat lobortis.
- Nulla porttitor vulputate libero.
  . Fusce euismod commodo velit.
  . Vivamus fringilla mi eu lacus.

|==================================
.....................................................................


.Cell containing lots of example markup elements
|====================================================================
|'URLs':
https://asciidoc3.org/[The AsciiDoc3 home page],
https://asciidoc3.org/,
mailto:joe.bloggs@example.com[email Joe Bloggs],
joe.bloggs@example.com,
callto:joe.bloggs[].

'Link': See <<X1,AsciiDoc3 source>>.

'Emphasized text', *Strong text*, +Monospaced text+, ``Quoted text''.

'Subscripts and superscripts': e^{amp}#960;i^+1 = 0. H~2~O and x^10^.
Some ^super text^ and ~some sub text~

'Replacements': (C) copyright, (TM) trademark, (R) registered trademark,
-- em dash, ... ellipsis, -> right arrow, <- left arrow, => right
double arrow, <= left double arrow.
|====================================================================

[[X1]]
.AsciiDoc3 source
---------------------------------------------------------------------
|====================================================================
|'URLs':
https://asciidoc3.org/[The AsciiDoc3 home page],
https://asciidoc3.org/,
mailto:joe.bloggs@example.com[email Joe Bloggs],
joe.bloggs@example.com,
callto:joe.bloggs[].

'Link': See <<X1,AsciiDoc3 source>>.

'Emphasized text', *Strong text*, +Monospaced text+, ``Quoted text''.

'Subscripts and superscripts': e^{amp}#960;i^+1 = 0. H~2~O and x^10^.
Some ^super text^ and ~some sub text~

'Replacements': (C) copyright, (TM) trademark, (R) registered trademark,
-- em dash, ... ellipsis, -> right arrow, <- left arrow, => right
double arrow, <= left double arrow.
|====================================================================
---------------------------------------------------------------------


.Spans, alignments and styles
[cols="e,m,^,>s",width="25%"]
|================
|1 >s|2 |3 |4
^|5 2.2+^.^|6 .3+<.>m|7
^|8
|9 2+>|10
|================

.AsciiDoc3 source
---------------------------------------------------------------------
.Spans, alignments and styles
[cols="e,m,^,>s",width="25%"]
|================
|1 >s|2 |3 |4
^|5 2.2+^.^|6 .3+<.>m|7
^|8
|9 2+>|10
|================
---------------------------------------------------------------------

.Three panes
[cols="a,2a"]
|==================================
|
[big]*Top Left Pane* +
Consul *necessitatibus* per id,
consetetur, eu pro everti postulant
homero verear ea mea, qui.

Consul *necessitatibus* per id,
consetetur, eu pro everti postulant
homero verear ea mea, qui.

.2+|
[big]*Right Pane* +
Consul *necessitatibus* per id,
consetetur, eu pro everti postulant
homero verear ea mea, qui.

-----------------------------------
Consul *necessitatibus* per id,
consetetur, eu pro everti postulant
homero verear ea mea, qui.
-----------------------------------

.Code filter example
[source,python]
-----------------------------------
''' A multi-line
    comment.'''
def sub_word(mo):
    ''' Single line comment.'''
    word = mo.group('word')
    if word in keywords[language]:
        return quote + word + quote
    else:
        return word
-----------------------------------

- Lorem ipsum dolor sit amet,
  consectetuer adipiscing elit.
  * Fusce euismod commodo velit.
  * Qui in magna commodo, est labitur
    dolorum an. Est ne magna primis
    adolescens. Sit munere ponderum
    dignissim et. Minim luptatum et vel.
  * Vivamus fringilla mi eu lacus.
  * Donec eget arcu bibendum nunc
    consequat lobortis.
- Nulla porttitor vulputate libero.
  . Fusce euismod commodo velit.
  . Vivamus fringilla mi eu lacus.

|
[big]*Bottom Left Pane* +
Consul *necessitatibus* per id,
consetetur, eu pro everti postulant
homero verear ea mea, qui.

Consul *necessitatibus* per id,
consetetur, eu pro everti postulant
homero verear ea mea, qui.

- Lorem ipsum dolor sit amet,
  consectetuer adipiscing elit.
  * Fusce euismod commodo velit.
  * Qui in magna commodo, est labitur
    dolorum an. Est ne magna primis
    adolescens. Sit munere ponderum
    dignissim et. Minim luptatum et vel.
  * Vivamus fringilla mi eu lacus.
  * Donec eget arcu bibendum nunc
    consequat lobortis.
- Nulla porttitor vulputate libero.
  . Fusce euismod commodo velit.
  . Vivamus fringilla mi eu lacus.

|==================================

.AsciiDoc3 source
[listing]
.....................................................................
.Three panes
[cols="a,2a"]
|==================================
|
[float]
Top Left Pane
~~~~~~~~~~~~~
Consul *necessitatibus* per id,
consetetur, eu pro everti postulant
homero verear ea mea, qui.

Consul *necessitatibus* per id,
consetetur, eu pro everti postulant
homero verear ea mea, qui.

.2+|
[float]
Right Pane
~~~~~~~~~~
Consul *necessitatibus* per id,
consetetur, eu pro everti postulant
homero verear ea mea, qui.

-----------------------------------
Consul *necessitatibus* per id,
consetetur, eu pro everti postulant
homero verear ea mea, qui.
-----------------------------------

.Code filter example
[source,python]
-----------------------------------
''' A multi-line
    comment.'''
def sub_word(mo):
    ''' Single line comment.'''
    word = mo.group('word')
    if word in keywords[language]:
        return quote + word + quote
    else:
        return word
-----------------------------------

- Lorem ipsum dolor sit amet,
  consectetuer adipiscing elit.
  * Fusce euismod commodo velit.
  * Qui in magna commodo, est labitur
    dolorum an. Est ne magna primis
    adolescens. Sit munere ponderum
    dignissim et. Minim luptatum et vel.
  * Vivamus fringilla mi eu lacus.
  * Donec eget arcu bibendum nunc
    consequat lobortis.
- Nulla porttitor vulputate libero.
  . Fusce euismod commodo velit.
  . Vivamus fringilla mi eu lacus.

|
[float]
Bottom Left Pane
~~~~~~~~~~~~~~~~
Consul *necessitatibus* per id,
consetetur, eu pro everti postulant
homero verear ea mea, qui.

Consul *necessitatibus* per id,
consetetur, eu pro everti postulant
homero verear ea mea, qui.

- Lorem ipsum dolor sit amet,
  consectetuer adipiscing elit.
  * Fusce euismod commodo velit.
  * Qui in magna commodo, est labitur
    dolorum an. Est ne magna primis
    adolescens. Sit munere ponderum
    dignissim et. Minim luptatum et vel.
  * Vivamus fringilla mi eu lacus.
  * Donec eget arcu bibendum nunc
    consequat lobortis.
- Nulla porttitor vulputate libero.
  . Fusce euismod commodo velit.
  . Vivamus fringilla mi eu lacus.

|==================================
.....................................................................


== Combinations of 'align', 'frame', 'grid', 'valign' and 'halign' attributes

:frame: all
:grid: all
:halign: left
:valign: top

[options="header"]
|====
||frame | grid |valign |halign
v|&nbsp;
&nbsp;
&nbsp;
|{frame} | {grid} |{valign} |{halign}
|====

.AsciiDoc3 source
---------------------------------------------------------------------
:frame: all
:grid: all
:halign: left
:valign: top

[options="header"]
|====
||frame | grid |valign |halign
v|&nbsp;
&nbsp;
&nbsp;
|{frame} | {grid} |{valign} |{halign}
|====
---------------------------------------------------------------------


:frame: sides
:grid: rows
:halign: center
:valign: middle

.Table test
[width="50%",options="header"]
|====
||frame | grid |valign |halign
v|&nbsp;
&nbsp;
&nbsp;
|{frame} | {grid} |{valign} |{halign}
|====

.AsciiDoc3 source
---------------------------------------------------------------------
:frame: sides
:grid: rows
:halign: center
:valign: middle

.Table test
[width="50%",options="header"]
|====
||frame | grid |valign |halign
v|&nbsp;
&nbsp;
&nbsp;
|{frame} | {grid} |{valign} |{halign}
|====
---------------------------------------------------------------------


:frame: topbot
:grid: cols
:halign: right
:valign: bottom

[align="right",width="50%",options="header"]
|====
||frame | grid |valign |halign
v|&nbsp;
&nbsp;
&nbsp;
|{frame} | {grid} |{valign} |{halign}
|====

.AsciiDoc3 source
---------------------------------------------------------------------
:frame: topbot
:grid: cols
:halign: right
:valign: bottom

[align="right",width="50%",options="header"]
|====
||frame | grid |valign |halign
v|&nbsp;
&nbsp;
&nbsp;
|{frame} | {grid} |{valign} |{halign}
|====
---------------------------------------------------------------------


:frame: none
:grid: none
:halign: left
:valign: top

[align="center",width="50%",options="header"]
|====
||frame | grid |valign |halign
v|&nbsp;
&nbsp;
&nbsp;
|{frame} | {grid} |{valign} |{halign}
|====

.AsciiDoc3 source
---------------------------------------------------------------------
:frame: none
:grid: none
:halign: left
:valign: top

[align="center",width="50%",options="header"]
|====
||frame | grid |valign |halign
v|&nbsp;
&nbsp;
&nbsp;
|{frame} | {grid} |{valign} |{halign}
|====
---------------------------------------------------------------------

:frame!:
:grid!:
:halign!:
:valign!:

